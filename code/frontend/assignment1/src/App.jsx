import { useState } from 'react'
import './App.css'

function App() {
  const [a1, setA] = useState();
  const [n, setN] = useState();
  const [sumResult, setSumResult] = useState();

  const handleInputChange = (e) => {
    if (e.target.id === 'a1') {
      setA(e.target.value);
    }
    if (e.target.id === 'n') {
      setN(e.target.value);
    }
  };

  const sendDataToServer = async () => {
    try {
      const response = await fetch('http://127.0.0.1:8000/api/get-all-vars', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ a1, n }),
      });

      const data = await response.json();
      if (response.ok) {
        // Update the state with the received sum_result
        setSumResult(data.sum_result);
      } else {
        console.error('Server error:', data.message);
      }
    } catch (error) {
      console.error('Error sending data to the server:', error.message);
    }
  };
  
  const handleSubmit = () => {
    console.log('testing: Value of a:', a1, 'Value of n:', n);
    sendDataToServer();
  };

  return (
    <>
      <div>
        <hr />
        <p className="Instructions">Please enter numbers and then click the Submit button:</p>
        <label>
          a1:
          <input
            type='number'
            id='a1'
            value={a1}
            onChange={handleInputChange}
          />
        </label>
        <br />
        <label>
          n:
          <input
            type='number'
            id='n'
            value={n}
            onChange={handleInputChange}
          />
        </label>
        <br />
        <button className='Submit' onClick={handleSubmit}>Submit</button>
        <hr />
        
        {sumResult && (
          <p className="Result"> Linear Sum Result: {sumResult}</p>
        )}
      </div>
    </>
  );
}

export default App;
