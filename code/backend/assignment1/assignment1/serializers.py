from rest_framework import serializers
from assignment1.models import LinearSeries

class LinearSeriesSerializers(serializers.ModelSerializer):
    class Meta:
        model = LinearSeries
        fields = '__all__'