from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from .models import LinearSeries
from .serializers import LinearSeriesSerializers
from django.db import connection

@api_view(['GET', 'POST'])
def get_all_vars(request):
    if request.method == 'GET':
        all_vars = LinearSeries.objects.all()
        serializer = LinearSeriesSerializers(all_vars, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
        
    if request.method == 'POST':
        if LinearSeries.objects.count() >= 5:
            LinearSeries.objects.all().delete()
        with connection.cursor() as cursor:
                cursor.execute("ALTER TABLE assignment1_linearseries AUTO_INCREMENT = 1")
        a1 = int(request.data.get('a1'))
        n = int(request.data.get('n'))
        sum_result = a1
        count= a1
        
        for i in range(1, n): #finding the sum of the linear series that starts with the number a and contains n numbers
            count +=1
            sum_result += count

        print(f"a1={a1}, n={n}, sum_result={sum_result} (testing)")
        LinearSeries.objects.create(a1=a1, n=n, SumOfLinearSeries=sum_result)

        
    

        return Response({"message": "Data received successfully", "sum_result": sum_result})
