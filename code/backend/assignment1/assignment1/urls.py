from django.contrib import admin
from django.urls import path
from assignment1 import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/get-all-vars', views.get_all_vars),
]
